""" Util functions related to the local system. """
# Python libs.
import os
import werkzeug
# Project files.
from config import CONFIGURATION

LOGGER = CONFIGURATION.get_logger(__name__)

def save_file(file_obj):
    """ Saves the given file and returns the file path. """
    file_name = werkzeug.utils.secure_filename(file_obj.filename)
    try:
        file_type = file_name.split('.')[1]
    except (IndexError, AttributeError) as e:
        LOGGER.error(f'{file_name} has no type. {e}')
    file_path = f'downloads/{file_name}'
    try:
        if not os.path.exists(os.path.realpath(file_path)):
            file_obj.save(file_path)
            LOGGER.debug(f'New file {file_name} saved at {file_path}')
    except AttributeError as e:
        LOGGER.error(f"File cannot be saved. {e}")

    return file_path