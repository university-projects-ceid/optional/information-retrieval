""" Helper functions for movies rest api. """
# Python libs.
import csv
import werkzeug
import json
# Project libs.
from config import CONFIGURATION
from src.utils.local_system import save_file
from src.models.movie.movie import Movie
from src.modules.ratings import lib as RATING_LIB

LOGGER = CONFIGURATION.get_logger(__name__)
ELASTIC = CONFIGURATION.get_elastic_instance()

#########################################
#        Question 1 Search              #
#########################################

def get_search_results(movie_params):
    """ Return elastic search results according to string for question 1. """
    response = ELASTIC.search(index="movie_index", body={"query": {"match": {**movie_params}}}, size=1000, ignore=404)

    return response

#########################################
#        Question 2 Search              #
#########################################

def get_search_results_question2(movie_params, user_id):
    """ Return elastic search results according to string for question 2. """
    search_results = get_search_results(movie_params)
    # Check if search results contain hits and get the movies found or return none
    if search_results.get('hits', None):
        search_results = search_results['hits']['hits']
    else:
        return None
    for movie_index, movie in enumerate(search_results):
        elastic_metric = movie['_score']
        user_rating = RATING_LIB.get_user_rating(user_id, movie['_id'])
        average_rating = RATING_LIB.get_avg_rating(movie['_id'])
        if elastic_metric and average_rating:
            if user_rating:
                search_results[movie_index]['new_metric'] = (float(elastic_metric) + float(user_rating['rating']) + float(average_rating)) / 3
            else:
                search_results[movie_index]['new_metric'] = (float(elastic_metric) + float(average_rating)) / 2
        else:
            search_results[movie_index]['new_metric'] = -1

    search_results.sort(key=lambda obj: float(obj['new_metric']), reverse=True)

    return search_results

#########################################
#        Question 3 Search              #
#########################################

def get_search_results_question3(movie_params, user_id):
    """ Return elastic search results according to string for question 3. """
    search_results = get_search_results(movie_params)
    LOGGER.debug(search_results)
    # Check if search results contain hits and get the movies found or return none
    if search_results.get('hits', None):
        search_results = search_results['hits']['hits']
    else:
        return None
    for movie_index, movie in enumerate(search_results):
        elastic_metric = movie['_score']
        user_rating = RATING_LIB.get_user_rating(user_id, movie['_id'])
        user_rating = user_rating.get('rating', None)
        average_rating = RATING_LIB.get_avg_rating(movie['_id'])
        if elastic_metric and average_rating:
            if not user_rating:
                user_rating = RATING_LIB.get_avg_cluster_rating(user_id, movie)
                if not user_rating:
                    search_results[movie_index]['new_metric'] = (float(elastic_metric) + float(average_rating)) / 2
            if user_rating:
                search_results[movie_index]['new_metric'] = (float(elastic_metric) + float(user_rating) + float(average_rating)) / 3
        else:
            search_results[movie_index]['new_metric'] = -1
    # Order the search result based on the new metrix
    search_results.sort(key=lambda obj: float(obj['new_metric']), reverse=True)

    return search_results

#########################################
#        Question 4 Search              #
#########################################

def get_search_results_question4(movie_params, user_id):
    """ Return elastic search results according to string for question 3. """
    search_results = get_search_results(movie_params)
    # Check if search results contain hits and get the movies found or return none
    if search_results.get('hits', None):
        search_results = search_results['hits']['hits']
    else:
        return None
    for movie_index, movie in enumerate(search_results):
        elastic_metric = movie['_score']
        user_rating = RATING_LIB.get_user_rating(user_id, movie['_id'])
        user_rating = user_rating['rating']
        average_rating = RATING_LIB.get_avg_rating(movie['_id'])
        if elastic_metric and average_rating:
            if not user_rating:
                # GET ML PREDICTION FOR USER.
                user_rating = get_movie_rating_prediction(user_id, movie['_id'])
            search_results[movie_index]['new_metric'] = (float(elastic_metric) + float(user_rating) + float(average_rating)) / 3
        else:
            search_results[movie_index]['new_metric'] = -1
    # Order the search result based on the new metrix
    search_results.sort(key=lambda obj: float(obj['new_metric']), reverse=True)

    return search_results

def get_movie_rating_prediction(user_id, movie_id):
    """ Return the movie rating prediction from user. """
    # NEED TO STORE TO DB MOVIES
    # CODE : under new key moivies:<movie_id>

    # NEED TO ONE HOT ENCODE MOVIE GENRES
    # CODE:  a = pd.DataFrame([{"movie": 'Killing', 'genre': ['Action']}, {"movie": 'Funny Killing', 'genre': ['Action', 'Comedy']}, {"movie": 'Funny', 'genre': ['Comedy']} ])
    # one_hot_enc_dataframe =  pd.get_dummies(a.genre.apply(pd.Series), prefix="", prefix_sep="")
    
    # NEED TO GET FROM MOVIE TITLE THE TF-IDF Vector
    # CODE: create new movie index and set parameter store_term_vectors to true.
    # GET TF, DF params and calculate tfifd = tf* log(N/DF)

    # Train the model based on tfidf, rating, onehotenc and predict the new movie_ratings.

    return 1

def store_movies_to_elastic(file_path):
    """
    Takes as input a csv file and stores all it's info in 
    elastic search.
    """
    movies_list = []
    line_count = 0
    # Read the csv file and extract all movies.
    with open (file_path, mode='r', encoding='utf8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            # First line of doc contains column titles, so skip it.
            if line_count == 0:
                line_count += 1
            # Movie genres should be represented as list.
            movie_genres = row['genres'].split('|')
            # If there are no genres listed for the movie, type none.
            if 'no genre' in movie_genres[0]:
                movie_genres = None
            # Add each movie to a list.
            movies_list.append({
                'id': row['movieId'],
                'title': row['title'], 
                'genres': movie_genres
                })

    # Create the mappings in elasticsearch.
    for movie in movies_list:
        es_movie = Movie(movie_id=int(movie['id']), movie_title=movie['title'], movie_genres=movie['genres'])
        response = ELASTIC.index(Movie.es_index, body=es_movie.json_str(), id=es_movie.id)

    return movies_list

def save_movie_file(file_obj):
    """ Saves the given file. """
    file_path = save_file(file_obj)

    return file_path

def get_index_info(index_name):
    """ Return index info. """
    response = ELASTIC.indices.get(index_name, ignore=[400, 404])
    if response.get('error', None):
        return None

    return response

def delete_index(index_name):
    """ Return index info. """
    response = ELASTIC.indices.delete(index_name, ignore=[400, 404])
    if response.get('error', None):
        return None

    return response

def create_index(index_name):
    """ Create a new index with the configuration specified in config yaml file. """
    response = ELASTIC.indices.create(index_name, body=CONFIGURATION.get_es_index_settings(), ignore=[400, 403, 404])
    if response.get('error', None):
        return None

    return response

def remove_all_from_index(index_name):
    """ 
    Remove everything from an index.
    Can be used with multiple if they are given as list.
    """
    response = ELASTIC.delete_by_query(index=index_name, body = {"query": {"match_all":{}}})

    return response