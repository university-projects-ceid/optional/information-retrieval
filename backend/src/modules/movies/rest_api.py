""" Movies file api module. """
# Python libs.
import werkzeug
import logging
import flask
from flask_restplus import Resource, Namespace
# Project files.
from . import lib
from config import CONFIGURATION

NAMESPACE = Namespace(
    'movie', description='Api namespace relevant to the movies.')
LOGGER = CONFIGURATION.get_logger(__name__)
# Models
movies_model = NAMESPACE.parser()
movies_model.add_argument('movies-file', type=werkzeug.datastructures.FileStorage,
                       help='Movies csv file to be parsed', location='files', required=True)

@NAMESPACE.route('/index')
@NAMESPACE.param('index_name', description='The index name to get info from')
class MovieIndex(Resource):
    """ Class representing an es movie index. """
    def get(self):
        """ Get info about an index. """
        args = flask.request.args
        index_name = args.get('index_name', None)
        response = self._sanitize_function_return(index_name, lib.get_index_info)

        return response

    def post(self):
        """ Create a new index. """
        args = flask.request.args
        index_name = args.get('index_name', None)
        response = self._sanitize_function_return(index_name, lib.create_index)

        return response

    def delete(self):
        """ Delete an index. """
        args = flask.request.args
        index_name = args.get('index_name', None)
        response = self._sanitize_function_return(index_name, lib.delete_index)

        return response
    
    def _sanitize_function_return(self, index_name, index_function):
        """
        Calculate response according to index_function using index_name 
        or return an index not found response.
        """
        if index_name:
            response = index_function(index_name)
            response = response if response else 'Index not found.'
        else:
            response = 'Index name was not given.'

        return response

@NAMESPACE.route('/file')
class MovieFile(Resource):
    """
    Api class representing the movies file.
    """
    @NAMESPACE.expect(movies_model)
    def post(self):
        """
        Input a new csv file and store information to db.
        """
        movies_file = movies_model.parse_args()['movies-file']
        file_path = lib.save_movie_file(movies_file)
        movies = lib.store_movies_to_elastic(file_path)
        # Response
        return movies

@NAMESPACE.route('/all')
class Movies(Resource):
    """
    Api class representing all movies.
    """
    @NAMESPACE.param('index_name', description='Name of the index or list that will be used.')
    def delete(self):
        """
        Remove everything from an index.
        """
        args = flask.request.args
        index_name = args.get('index_name', None)
        response = lib.remove_all_from_index(index_name)
        # Response
        return response

@NAMESPACE.route('/search')
@NAMESPACE.param('index_name', description='The index name to get info from')
class MovieSearch(Resource):
    """
    Api class representing a movie search.
    """
    @NAMESPACE.param('movie_title', description='The title of the movie')
    @NAMESPACE.param('movie_id', description='The id of the movie')
    @NAMESPACE.param('movie_genres', description='The genres of the movie')
    def get(self):
        """
        Get all movies according to string and how
        relevant they are.
        """
        args = flask.request.args
        movie_params = {}
        movie_params['title'] = args.get('movie_title', None)
        movie_params['_id'] = args.get('movie_id', None)
        # Make movie Id an int
        if movie_params['_id']:
            movie_params['_id'] = int(movie_params['_id'])    
        movie_params['genres'] = args.get('movie_genres', None)
        movie_params = {movie_param: value for movie_param, value in movie_params.items() if value is not None}

        response = lib.get_search_results(movie_params)
        # Get only hits from resposnse
        if response.get('hits'):
            response = response['hits']['hits']
        else:
            response = 'Nothing found.'
        # Response
        return response

@NAMESPACE.route('/search/question2')
@NAMESPACE.param('index_name', description='The index name to get info from')
class MovieSearchQuestion2(Resource):
    """
    Api class representing a movie search for question 2.
    """
    @NAMESPACE.param('user_id', description='The id of the user')
    @NAMESPACE.param('movie_title', description='The title of the movie')
    @NAMESPACE.param('movie_id', description='The id of the movie')
    @NAMESPACE.param('movie_genres', description='The genres of the movie')
    def get(self):
        """
        Get all movies according to string and how
        relevant they are.
        """
        args = flask.request.args
        user_id = args.get('user_id', None)
        if not user_id:
            return 'User id not set.'
        movie_params = {}
        movie_params['title'] = args.get('movie_title', None)
        movie_params['_id'] = args.get('movie_id', None)
        # Make movie Id an int
        if movie_params['_id']:
            movie_params['_id'] = int(movie_params['_id'])    
        movie_params['genres'] = args.get('movie_genres', None)
        movie_params = {movie_param: value for movie_param, value in movie_params.items() if value is not None}

        response = lib.get_search_results_question2(movie_params=movie_params, user_id=user_id)
        # Response
        return response

@NAMESPACE.route('/search/question3')
@NAMESPACE.param('index_name', description='The index name to get info from')
class MovieSearchQuestion3(Resource):
    """
    Api class representing a movie search for question 3.
    """
    @NAMESPACE.param('user_id', description='The id of the user')
    @NAMESPACE.param('movie_title', description='The title of the movie')
    @NAMESPACE.param('movie_id', description='The id of the movie')
    @NAMESPACE.param('movie_genres', description='The genres of the movie')
    def get(self):
        """
        Get all movies according to string and how
        relevant they are.
        """
        args = flask.request.args
        user_id = args.get('user_id', None)
        if not user_id:
            return 'User id not set.'
        movie_params = {}
        movie_params['title'] = args.get('movie_title', None)
        movie_params['_id'] = args.get('movie_id', None)
        # Make movie Id an int
        if movie_params['_id']:
            movie_params['_id'] = int(movie_params['_id'])    
        movie_params['genres'] = args.get('movie_genres', None)
        movie_params = {movie_param: value for movie_param, value in movie_params.items() if value is not None}

        response = lib.get_search_results_question3(movie_params=movie_params, user_id=user_id)
        # Response
        return response

@NAMESPACE.route('/search/question4')
@NAMESPACE.param('index_name', description='The index name to get info from')
class MovieSearchQuestion4(Resource):
    """
    Api class representing a movie search for question 4.
    """
    @NAMESPACE.param('user_id', description='The id of the user')
    @NAMESPACE.param('movie_title', description='The title of the movie')
    @NAMESPACE.param('movie_id', description='The id of the movie')
    @NAMESPACE.param('movie_genres', description='The genres of the movie')
    def get(self):
        """
        Get all movies according to string and how
        relevant they are.
        """
        args = flask.request.args
        user_id = args.get('user_id', None)
        if not user_id:
            return 'User id not set.'
        movie_params = {}
        movie_params['title'] = args.get('movie_title', None)
        movie_params['_id'] = args.get('movie_id', None)
        # Make movie Id an int
        if movie_params['_id']:
            movie_params['_id'] = int(movie_params['_id'])    
        movie_params['genres'] = args.get('movie_genres', None)
        movie_params = {movie_param: value for movie_param, value in movie_params.items() if value is not None}

        response = lib.get_search_results_question4(movie_params=movie_params, user_id=user_id)
        # Response
        return response
