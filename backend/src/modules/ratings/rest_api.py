""" Rest api for ratings. """
# Python libs.
import werkzeug
import logging
import flask
from flask_restplus import Resource, Namespace
# Project files.
from . import lib
from config import CONFIGURATION

NAMESPACE = Namespace(
    'rating', description='Api namespace relevant to the movie ratings.')
LOGGER = CONFIGURATION.get_logger(__name__)
# Models
ratings_model = NAMESPACE.parser()
ratings_model.add_argument('ratings-file', type=werkzeug.datastructures.FileStorage,
                       help='Ratings csv file to be parsed', location='files', required=True)
ratings_model.add_argument('ratings-file', type=werkzeug.datastructures.FileStorage,
                       help='Ratings csv file to be parsed', location='files', required=True)
ratings_model.add_argument('movies-file', type=werkzeug.datastructures.FileStorage,
                       help='Movies csv file to be parsed', location='files', required=False)


@NAMESPACE.route('/')
class MovieFile(Resource):
    """
    Api class representing one rating.
    """
    @NAMESPACE.param('user_id', description='User id that rated')
    @NAMESPACE.param('movie_id', description='Movie id that was rated')
    def get(self):
        """
        Get a rating from database.
        """
        args = flask.request.args
        user_id = args.get('user_id', None)
        movie_id = args.get('movie_id', None)

        response = 'Input arguments are not correct.'
        if user_id and movie_id:
            response = lib.get_user_rating(user_id, movie_id)
        # Response
        return response

@NAMESPACE.route('/file')
class MovieFile(Resource):
    """
    Api class representing the ratings file.
    """
    @NAMESPACE.expect(ratings_model)
    def post(self):
        """
        Input a new csv file and store information to db.
        """
        ratings_file = ratings_model.parse_args()['ratings-file']
        file_path = lib.save_ratings_file(ratings_file)
        response = lib.store_ratings_to_db(file_path)
        # Response
        return response

@NAMESPACE.route('/all')
class MovieFile(Resource):
    """
    Api class representing all the ratings.
    """
    def delete(self):
        """
        Delete all ratings from db.
        """
        response = lib.drop_db()
        # Response
        return response

@NAMESPACE.route('/clustering')
class MovieClustering(Resource):
    """
    Api class responsible for user clustering according to ratings.
    """
    @NAMESPACE.expect(ratings_model)
    def post(self):
        """
        Seperate users in clusters according to ratings and store
        to db.
        """
        ratings_file = ratings_model.parse_args()['ratings-file']
        movies_file = ratings_model.parse_args()['movies-file']
        ratings_path = lib.save_ratings_file(ratings_file)
        movies_path = lib.save_ratings_file(movies_file)
        response = lib.store_clusters_to_db(ratings_path, movies_path)
        # Response
        return response