""" Helper functions for ratings rest api. """
# Python libs.
import csv
import pandas as pd
# Project libs.
from src.utils.local_system import save_file
from src.models.rating.rating import Rating as RatingModel, UserRatingCluster as UserRatingClusterModel
from config import CONFIGURATION

LOGGER = CONFIGURATION.get_logger(__name__)

def save_ratings_file(file_obj):
    """ Saves the given file. """
    file_path = save_file(file_obj)

    return file_path

def store_ratings_to_db(file_path):
    """ Store ratings to database. """
    ratings_list = extract_ratings_from_file(file_path)
    for rating in ratings_list:
        for user_id, ratings in rating.items():
            rating_model = RatingModel(user_id, ratings)
            rating_model.save_to_db()
    
    return True

def store_clusters_to_db(ratings_path, movies_path):
    """
    Get ratings from file, perform clustering and store clusters in 
    db.
    """
    ratings = get_ratings_movies_matrix(ratings_path, movies_path)
    cluster_model = UserRatingClusterModel(ratings)
    cluster_model.save_to_db()

    return True

def get_ratings_movies_matrix(ratings_path, movie_path):
    """ Return a pandas matrix with the rating movies. """
    ratings = pd.read_csv(ratings_path)
    movies = pd.read_csv(movie_path)
    ratings = pd.merge(ratings, movies[['movieId', 'title']], on='movieId' )

    return ratings

def extract_ratings_from_file(file_path):
    """
    Get all ratings per user from csv file and return a list with each 
    one.
    """
    ratings_list = []
    user_id_set = ()
    line_count = 0
    # Read the csv file and extract all ratings.
    with open (file_path, mode='r', encoding='utf8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        user_rating = {}
        for row in csv_reader:
            # First line of csv contains column titles, so skip it.
            if line_count == 0:
                line_count += 1
            # Get user rating or none if ratings list is empty or user rating not exists.
            index, user_rating_temp = next(([index, obj] for index, obj in enumerate(ratings_list) if row['userId'] in obj), [None, None])
            if user_rating_temp:
                ratings_list.pop(int(index))
                user_rating = user_rating_temp
            else:
                user_rating = {}
            # Append into the user id value list a movie rating.
            user_rating.setdefault(row['userId'], []).append({
                'movie_id': int(row['movieId']), 
                'rating': float(row['rating']),
                'timestamp': row['timestamp']
                })
            ratings_list.append(user_rating)
    
    return ratings_list

def drop_db():
    """ Drop database. """
    reponse = RatingModel.drop_db()

    return reponse

def get_user_rating(user_id, movie_id):
    """ Return user's movie rating.  """
    rating = {}
    if user_id and movie_id:
        rating = RatingModel.get_user_rating(user_id, movie_id)

    return rating

def get_avg_rating(movie_id):
    """ Return average movie rating.  """
    avg_rating = None
    if movie_id:
        avg_rating = RatingModel.get_avg_rating(movie_id)

    return avg_rating

def get_avg_cluster_rating(user_id, movie):
    """ Get average cluster rating. """
    avg_cluster_rating = UserRatingClusterModel.get_avg_cluster_rating(user_id, movie)

    return avg_cluster_rating