from flask_restplus import Resource, Api
from .movies.rest_api import NAMESPACE as movies_ns
from .ratings.rest_api import NAMESPACE as ratings_ns


API = Api(doc='/doc/', prefix='/api')

API.add_namespace(movies_ns)
API.add_namespace(ratings_ns)
