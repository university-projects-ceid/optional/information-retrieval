""" Movies elastisearch model. """
# Python libs.
import json
# Project libs.
from config import CONFIGURATION

ELASTIC = CONFIGURATION.get_elastic_instance()
LOGGER = CONFIGURATION.get_logger(__name__)

class Movie():
    """ Class representing a Movie model for elasticsearch. """
    es_index = "movie_index"
    def __init__(self, movie_id, movie_title, movie_genres):
        """ Class constructor. """
        self.id = movie_id
        self.title = movie_title
        self.genres = movie_genres
        # Class is intatiated with an empty json.

    def json_str(self):
        """ Build a json string for elastic to index. """
        source = {
            'title': self.title,
            'genres': self.genres
        }
        # Attempt to create a JSON string of the document using json.dumps()
        try:
        # Use the 'indent' parameter with json.dumps() for readable JSON
            json_str = json.dumps(source, indent=4)
        except Exception as e:
            json_str = "{}"
            LOGGER.error(f"Document json_string() ERROR: {e}")
        
        return json_str
