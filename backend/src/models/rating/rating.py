""" Module for the rating redis model. """
# Python libs.
import pandas as pd
import numpy as np
from scipy.sparse import csr_matrix
from sklearn.cluster import KMeans
 # Project libs.
from config import CONFIGURATION

LOGGER = CONFIGURATION.get_logger(__name__)
# Get redis db connection.
redis_con =  CONFIGURATION.get_redis_con()

class Rating():
    """ Class representing a rating db object. """
    def __init__(self, user_id, movie_ratings):
        self.user_id = user_id
        self.movie_ratings = movie_ratings
    
    def  save_to_db(self):
        """ Store a rating to redis db. """
        # Add to ratings the user id.
        # Base redis key for ratings.
        redis_key = 'user'
        # Add to key user the user id.
        redis_con.sadd(redis_key, self.user_id)
        LOGGER.debug(f'Add to redis key: {redis_key} user id: {self.user_id}')
        # Add to key user:<user_id> the movie_rating set member.
        redis_key = ":".join((redis_key, str(self.user_id)))
        redis_con.sadd(redis_key, 'movie_rating')
        LOGGER.debug(f'Add to redis key: {redis_key} set element: movie_rating')
        # Add movie ratings object.
        redis_key = ":".join((redis_key, 'movie_rating'))
        for movie_rating in self.movie_ratings:
            # Under key: user:<user_id>:movie_rating add to set the movie id
            redis_con.sadd(redis_key, movie_rating['movie_id'])
            LOGGER.debug(f'Add to redis key: {redis_key} movie id: {movie_rating["movie_id"]}')
            temp_key = ":".join((redis_key, str(movie_rating['movie_id'])))
            movie_rating.pop('movie_id')
            redis_con.hmset(temp_key, movie_rating)
            LOGGER.debug(f'Add to redis key: {temp_key} user ratings: {movie_rating}')
    
    @classmethod
    def drop_db(cls):
        """ Drop database. """
        redis_con.flushdb()

        return 'Ok'

    @classmethod
    def get_user_rating(cls, user_id, movie_id):
        """ Return a user's movie rating. """
        # Convert user id to string.
        user_id = str(user_id)
        movie_id = str(movie_id)
        # Get movies rated by user id.
        redis_key = ":".join(('user', user_id, 'movie_rating'))
        movies_rated = redis_con.smembers(redis_key)
        LOGGER.debug(f'Get from key {redis_key} all movies rated.')
        # If user has rated the movie return rating else return empty dict
        rating = {}
        if movie_id in movies_rated:
            redis_key = ":".join((redis_key, movie_id))
            rating = redis_con.hgetall(redis_key)

        return rating
    
    @classmethod
    def get_avg_rating(cls, movie_id):
        """ Return the average movie rating. """
        movie_id = str(movie_id)
        # Get all keys that contain the movie_id.
        movie_keys = redis_con.keys(f'user*rating:{movie_id}')
        LOGGER.debug(f'Get all keys containg user*rating:{movie_id}: {movie_keys}.')
        # Get the number of the ratings.
        movie_rating_num = len(movie_keys)
        # If movie does not exist return 0.
        if not movie_keys:
            return 0
        avg_rating = 0
        # For each key get the rating of the movie and add to avg rating.
        for movie_key in movie_keys:
            movie_rating = redis_con.hget(movie_key, 'rating')
            LOGGER.debug(f'Get from key: {movie_key}: movie rating: {movie_rating}.')
            if not movie_rating: continue
            avg_rating += float(movie_rating)

        avg_rating /= movie_rating_num

        return avg_rating

class UserRatingCluster():
    number_of_clusters = 200
    """ Class representing a user cluster basaed on movie ratings. """
    def __init__(self, user_ratings):
        self.user_ratings = user_ratings
        self.clusters = self._get_clusters().tolist()
    
    def save_to_db(self):
        """ Save cluster to db. """
        # Need to update the user:<user_id> set with the string cluster,
        # in order to store each cluster number to each user.
        # Also create cluster key and store for each cluster id and correspodning
        # ratings to the cluster key.
        user_redis_key = 'user'
        cluster_redis_key = 'cluster'
        for index, cluster in enumerate(self.clusters):
            user_id = index + 1
            temp_user_key = ":".join((user_redis_key, str(user_id)))
            redis_con.sadd(temp_user_key, 'cluster')
            LOGGER.debug(f'Add to key {temp_user_key} set member string: cluster')
            temp_user_key = ":".join((temp_user_key, 'cluster'))
            redis_con.set(temp_user_key, cluster)
            LOGGER.debug(f'Add to key {temp_user_key} string: {cluster}')
            # Store all cluster ids under new cluster redis key.
            redis_con.sadd(cluster_redis_key, cluster)
            LOGGER.debug(f'Add to key {cluster_redis_key} cluster id: {cluster}')
            # Store to cluster:<cluster_id> key movie_rating set member string.
            temp_cluster_key = ":".join((cluster_redis_key, str(cluster)))
            redis_con.sadd(temp_cluster_key, 'movie_rating')
            LOGGER.debug(f'Add to key: {temp_cluster_key} set member: movie_rating')
            # Store to key: cluster:<cluster_id>:movie_rating the cluster rated movie ids.
            temp_movie_rated_key = ":".join((temp_cluster_key, 'movie_rating'))
            for index, row in self.user_ratings.iterrows():
                if user_id == row['userId']:
                    redis_con.sadd(temp_movie_rated_key, row['movieId'])
                    LOGGER.debug(f'Add to key: {temp_movie_rated_key} set member: {row["movieId"]}')
                    # Add to key cluster:<cluster_id>:movie_rating:<movie_id> the rating in the existing list.
                    movie_rating_key = ":".join((temp_movie_rated_key, str(row['movieId'])))
                    redis_con.lpush(movie_rating_key, row['rating'])
                    LOGGER.debug(f'Add to key: {movie_rating_key} rating: {str( row["rating"])}')

    def _get_clusters(self):
        """ Perform clustering on objects users ratings and return clusters. """
        # Kmeans needs a sparse matrix of the user ratings.
        user_rating_pivot = pd.pivot_table(self.user_ratings, index='userId', columns= 'title', values='rating')
        sparse_ratings = csr_matrix(pd.SparseDataFrame(user_rating_pivot).to_coo())
        predictions = KMeans(n_clusters=20, algorithm='full').fit_predict(sparse_ratings)

        return predictions
    
    @classmethod
    def get_avg_cluster_rating(cls, user_id, movie):
        """ Calculate movie average cluster rating. """
        # Get cluster id.
        redis_key = ":".join(('user', str(user_id), 'cluster'))
        cluster_id = redis_con.get(redis_key)
        # Get movie ratings
        movie_id = movie['_id']
        redis_key = ":".join(['cluster', str(cluster_id), 'movie_rating', str(movie_id)])
        movie_ratings = redis_con.lrange(redis_key, 0, -1)
        # Check if movie ratings is zero for division.
        if not isinstance(movie_ratings, list) or len(movie_ratings) == 0:
            return None

        avg_rating = sum([float(rating) for rating in movie_ratings]) / len(movie_ratings)

        return avg_rating