""" This module contains the Config class. """
import os.path
import yaml
import json
import redis
import logging
from elasticsearch import Elasticsearch

class Config():
    """ Add basic config class """

    def __init__(self):
        """ Class constructor """
        if os.path.isfile('config/config.yaml'):
            config_dict = yaml.safe_load(open('config/config.yaml', 'r'))
            # Backend settings.
            self.backend_ip = config_dict['development']['backend']['ip']
            self.backend_port = config_dict['development']['backend']['port']
            # Redis db settings
            self.redis_host = config_dict['development']['backend']['redis']['host']
            self.redis_port = config_dict['development']['backend']['redis']['port']
            self.redis_decoding = config_dict['development']['backend']['redis']['decoding']
            self.redis_db = config_dict['development']['backend']['redis']['db']
            # Elastic settings.
            self.es_host = config_dict['development']['backend']['elastic']['elastic_host']
            self.es_port = config_dict['development']['backend']['elastic']['elastic_port']
            self.es_index_settings = config_dict['development']['backend']['elastic']['settings']
            # Logging and debugging settings.
            self.debug_mode = config_dict['development']['backend']['debug_mode']
            self.logging_level = config_dict['development']['logging']['level']
            self.logging_format = config_dict['development']['logging']['format']
            self.logging_datefmt = config_dict['development']['logging']['datefmt']
            self.logging_folder = config_dict['development']['logging']['folder']
        else:
            print('No config yaml file found.')
            exit(1)
    
    def get_backend_socket(self):
        """Return the backend socket."""
        return ':'.join([self.backend_ip, str(self.backend_port)])

    def get_elastic_instance(self):
        """ Return elastic search instance. """
        elastic_instance = Elasticsearch([{'host': self.es_host, 'port': self.es_port}])
        
        return elastic_instance
    
    def get_es_index_settings(self):
        """
        Return elasticsearch index settings with default k1 and b values and 
        BM25 algorithm. 
        """
        settings = {'settings': self.es_index_settings}
        
        return json.dumps(settings)
    
    def get_redis_con(self):
        """ Get redis connection object. """
        redis_con = redis.Redis(host=self.redis_host, decode_responses=self.redis_decoding, db=self.redis_db, port=self.redis_port)

        return redis_con
    
    def get_logger(self, module_name):
        """ Set up logging and return logger name. """
        LOGGER = logging.getLogger(f'app:{module_name}')
        logging.basicConfig(format=self.logging_format, level=self.logging_level,
                            datefmt=self.logging_datefmt, filename=self.logging_folder, 
                            filemode='a')
        
        return LOGGER

CONFIGURATION = Config()
