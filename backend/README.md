# Information Retrieval School Porject Backend Installation Instructions.

## Requirements
For downloading the project dependencies you will need [pip](https://pypi.org/project/pip/) and python 3.6.x. A usefull tool for handling different python versions is [pyenv](https://github.com/pyenv/pyenv). Follow the links for more instrunctions on downloading the previous programs.

## Installation

### Backend Installation

First clone the repository.
```
git clone https://gitlab.com/university-projects-ceid/optional/information-retrieval.git
```
To download python dependencies you will need [pipenv](https://pipenv.readthedocs.io/en/latest/).
```
pip install pipenv
```
Navigate into the project folder and run the following command to download the dependencies.
```
cd information-retrieval/backend
pipenv install --dev
```
To run the project run:
```
pipenv shell
python app.py
```

### Redis installation

For installing the redis-server package run following commands"
```
sudo apt update
sudo apt install redis-server
```
After installation change the redis configuration in order to run as a service (on `Ubuntu`).
```
sudo nano /etc/redis/redis.conf
```
Change the supervised directive to use systemd:
```
# Note: these supervision methods only signal "process is ready."
#       They do not enable continuous liveness pings back to your supervisor.
supervised systemd
```
After that, restart service and run the redis-cli in order to check db.
```
sudo systemctl restart redis.service
redis-cli
```
In the project we use the second database of the 16 in the redis-server. So, run after the `redis-cli` command:
```
select 2
```
