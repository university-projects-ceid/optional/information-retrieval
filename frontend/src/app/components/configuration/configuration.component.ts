import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-configuration",
  templateUrl: "./configuration.component.html",
  styleUrls: ["./configuration.component.sass"]
})
export class ConfigurationComponent implements OnInit {
  public isCollapsedCreate = true;
  public isCollapsedMovies = true;
  public isCollapsedRatings = true;

  constructor() {}

  ngOnInit() {}
}
