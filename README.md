# Welcome to the Information Retrieval school project.

## Getting Started
This is a school project, created by George Tsoulos and Thanos Stamatakis.

## Installation Instructions

Backend
* https://gitlab.com/university-projects-ceid/optional/information-retrieval/tree/master/backend
  
Frontend

* https://gitlab.com/university-projects-ceid/optional/information-retrieval/tree/master/frontend